part of 'location_bloc.dart';

@immutable
abstract class LocationState {}

class LocationStateInitial extends LocationState {}

class LocationStateLoading extends LocationState {}

class LocationStateResult extends LocationState {
  final Position position;
  LocationStateResult({required this.position});
}

class LocationStateError extends LocationState {
  final String message;
  LocationStateError({required this.message});
}
