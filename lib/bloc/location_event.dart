part of 'location_bloc.dart';

@immutable
abstract class LocationEvent {}

class LocationEventCheckPermissions extends LocationEvent {}

class LocationEventGetPosition extends LocationEvent {}
