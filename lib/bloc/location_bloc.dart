import 'package:bloc/bloc.dart';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  LocationBloc() : super(LocationStateInitial()) {
    on<LocationEventCheckPermissions>(
      (event, emit) async {
        try {
          bool serviceEnabled;
          LocationPermission permission;

          /// Permission check
          serviceEnabled = await Geolocator.isLocationServiceEnabled();
          if (!serviceEnabled) {
            emit(
              LocationStateError(
                message: 'Location services are disabled',
              ),
            );
          }
          permission = await Geolocator.checkPermission();
          if (permission == LocationPermission.denied) {
            permission = await Geolocator.requestPermission();
            if (permission == LocationPermission.denied) {
              emit(
                LocationStateError(
                  message: 'Location permissions are denied',
                ),
              );
            }
          }
          if (permission == LocationPermission.deniedForever) {
            emit(
              LocationStateError(
                message: 'Location permissions are permanently denied, we cannot request permissions.',
              ),
            );
          }
        } catch (e) {
          emit(
            LocationStateError(
              message: e.toString(),
            ),
          );
        }
      },
    );

    /// Location
    on<LocationEventGetPosition>(
      (event, emit) async {
        emit(LocationStateLoading());
        try {
          var finalPosition = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high,
          );
          emit(
            LocationStateResult(
              position: finalPosition,
            ),
          );
        } catch (e) {
          emit(
            LocationStateError(
              message: e.toString(),
            ),
          );
        }
      },
    );
  }
}
