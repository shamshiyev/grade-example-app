import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/location_bloc.dart';

class GetLocationButton extends StatelessWidget {
  const GetLocationButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        const Text(
          'Tap to get Your location',
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
        ),
        const SizedBox(
          height: 16,
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24.0),
              ),
              padding: const EdgeInsets.all(8)),
          child: const Icon(
            Icons.my_location,
            size: 56,
          ),
          onPressed: () {
            BlocProvider.of<LocationBloc>(context).add(
              LocationEventGetPosition(),
            );
          },
        ),
        const Spacer(),
      ],
    );
  }
}
