import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grading_test_app/bloc/location_bloc.dart';
import 'package:grading_test_app/ui/widgets/get_location_button.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class YandexMapsTab extends StatefulWidget {
  const YandexMapsTab({super.key});

  @override
  State<StatefulWidget> createState() => YandexMapsTabState();
}

class YandexMapsTabState extends State<YandexMapsTab> with AutomaticKeepAliveClientMixin {
  final mapControllerCompleter = Completer<YandexMapController>();
  final List<MapObject> mapObjects = [];

  @override
  @mustCallSuper
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<LocationBloc, LocationState>(
      builder: (context, state) {
        if (state is LocationStateLoading) {
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.cyan,
            ),
          );
        }
        if (state is LocationStateError) {
          return AlertDialog(
            title: Text(state.message),
          );
        }
        if (state is LocationStateResult) {
          mapObjects.add(
            PlacemarkMapObject(
              mapId: const MapObjectId(''),
              point: Point(
                latitude: state.position.latitude,
                longitude: state.position.longitude,
              ),
              icon: PlacemarkIcon.single(
                PlacemarkIconStyle(
                  image: BitmapDescriptor.fromAssetImage(
                    'assets/yandex_marker.png',
                  ),
                  scale: 0.4,
                ),
              ),
            ),
          );
          return YandexMap(
            mapObjects: mapObjects,
            onMapCreated: (controller) {
              mapControllerCompleter.complete(controller);
              _moveToCurrentLocation(
                state.position.latitude,
                state.position.longitude,
              );
            },
          );
        }
        return const GetLocationButton();
      },
    );
  }

  Future<void> _moveToCurrentLocation(
    double latitude,
    double longitude,
  ) async {
    (await mapControllerCompleter.future).moveCamera(
      animation: const MapAnimation(
        type: MapAnimationType.smooth,
        duration: 2,
      ),
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: Point(
            latitude: latitude,
            longitude: longitude,
          ),
          zoom: 18,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
