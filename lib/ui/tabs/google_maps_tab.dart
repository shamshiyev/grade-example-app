import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:grading_test_app/bloc/location_bloc.dart';
import 'package:grading_test_app/ui/widgets/get_location_button.dart';

class GoogleMapsTab extends StatefulWidget {
  const GoogleMapsTab({super.key});

  @override
  State<StatefulWidget> createState() => GoogleMapsTabState();
}

class GoogleMapsTabState extends State<GoogleMapsTab> with AutomaticKeepAliveClientMixin {
  @override
  @mustCallSuper
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<LocationBloc, LocationState>(
      builder: (context, state) {
        if (state is LocationStateLoading) {
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.cyan,
            ),
          );
        }
        if (state is LocationStateError) {
          return AlertDialog(
            title: Text(state.message),
          );
        }
        if (state is LocationStateResult) {
          return GoogleMap(
            myLocationEnabled: true,
            initialCameraPosition: CameraPosition(
              target: LatLng(
                state.position.latitude,
                state.position.longitude,
              ),
              tilt: 12,
              zoom: 18,
            ),
          );
        }
        return const GetLocationButton();
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
