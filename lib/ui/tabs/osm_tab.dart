import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_osm_plugin/flutter_osm_plugin.dart';
import 'package:grading_test_app/bloc/location_bloc.dart';
import 'package:grading_test_app/ui/widgets/get_location_button.dart';

class OSMTab extends StatefulWidget {
  const OSMTab({super.key});

  @override
  State<StatefulWidget> createState() => OSMTabState();
}

class OSMTabState extends State<OSMTab> with AutomaticKeepAliveClientMixin {
  MapController controller = MapController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  @mustCallSuper
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<LocationBloc, LocationState>(
      builder: (context, state) {
        if (state is LocationStateLoading) {
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.cyan,
            ),
          );
        }
        if (state is LocationStateError) {
          return AlertDialog(
            title: Text(state.message),
          );
        }
        if (state is LocationStateResult) {
          return OSMFlutter(
            showContributorBadgeForOSM: true,
            onMapIsReady: (_) {
              controller.goToLocation(
                GeoPoint(
                  latitude: state.position.latitude,
                  longitude: state.position.longitude,
                ),
              );
            },
            initZoom: 18,
            controller: controller,
            userLocationMarker: UserLocationMaker(
              personMarker: const MarkerIcon(
                icon: Icon(
                  Icons.run_circle_outlined,
                  color: Colors.cyan,
                  size: 120,
                ),
              ),
              directionArrowMarker: const MarkerIcon(
                icon: Icon(
                  Icons.keyboard_arrow_up_outlined,
                ),
              ),
            ),
            trackMyPosition: true,
          );
        }
        return const GetLocationButton();
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
