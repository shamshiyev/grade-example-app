import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grading_test_app/ui/tabs/google_maps_tab.dart';
import 'package:grading_test_app/ui/tabs/osm_tab.dart';
import 'package:grading_test_app/ui/tabs/yandex_maps_tab.dart';

import '../bloc/location_bloc.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Get My Location'),
        ),
        body: const TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            OSMTab(),
            GoogleMapsTab(),
            YandexMapsTab(),
          ],
        ),
        bottomNavigationBar: BlocBuilder<LocationBloc, LocationState>(
          builder: (context, state) {
            return state is LocationStateResult
                ? const Padding(
                    padding: EdgeInsets.all(4),
                    child: TabBar(
                      tabs: [
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('OSM'),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('GoogleMaps'),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('Yandex'),
                        ),
                      ],
                    ),
                  )
                : const SizedBox.shrink();
          },
        ),
      ),
    );
  }
}
