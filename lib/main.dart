import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grading_test_app/bloc/location_bloc.dart';
import 'ui/home_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.cyan,
        canvasColor: Colors.white70,
        appBarTheme: const AppBarTheme(
          titleTextStyle: TextStyle(
            color: Colors.black45,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      home: BlocProvider<LocationBloc>(
        create: (context) => LocationBloc()
          ..add(
            LocationEventCheckPermissions(),
          ),
        child: const HomeScreen(),
      ),
    );
  }
}
